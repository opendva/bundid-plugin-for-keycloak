# BundID-Plugin für Keycloak

Zur Authentifizierung eines Nutzers via BundID (Nutzerkonto des Bundes) muss ein SAML-Request an Bund-ID gesendet werden. Dabei müssen gewisse BundID-spezifische Informationen im Extensions-Knoten des Request-XMLs aufgeführt werden. Dieses Plugin enthält eine Erweiterung für die Anwendung Keycloak derart, dass im Extension-Knoten Angaben bzw. Einschränkungen zu den Nutzerdaten-Typen, die die SAML-Response liefern soll, aufgeführt werden.

## Verwendung

Nach Deployment und Anwendungsstart muss in der _Keycloak Administration Console_ für den gewünschten Realm ein Identity Provider vom Typ `BundID` hinzugefügt werden.

![IDP-Auswahl BundID](doc/images/idp-choice.png)

Für jeden Nutzerdaten-Typ, zu dem in der SAML-Response von BundID Daten geliefert werden sollen, muss am Identity Provider ein _Identity Provider Mapper_ vom Typ `Attribute Importer` angelegt werden. Als Attribut-Name ist jeweils die URN des Nutzerdaten-Typs (im BundID-Kontext auch einfach nur _Attribut_ genannt) anzugeben. Unter _User Attribute Name_ wird der Key hinterlegt, über den man innerhalb von Keycloak z.B. in Client Scopes über weitere Mapper auf das Attribut zugreifen kann.

![Attribut-Mapper](doc/images/attr-mapper.png)

Wird der so angelegte Identity Provider für die Authentifizierung ausgewählt, so werden im Extension-Knoten des Request-XMLs für die Authentifizierung dann genau die Attribute aufgeführt, zu denen am Identity Provider die entsprechenden Attribute Mapper hinterlegt sind.

Zusätzlich müssen voraussichtlich ab Juli 2025 sowohl die BMI-ID als auch eine Anzeige-Information zur Seite, zu der man nach erfolgreicher BundID-Authentifizierung zurückgeleitet wird, mitgesendet werden. Für diese Informationen müssen ebenfalls Provider Mapper angelegt werden, und zwar vom Typ `Hardcoded Attribute`. Die Keys der beiden Mapper müssen _displayName_ und _bmiId_ lauten.

![Attribut-Mapper](doc/images/hardcoded-attr-1.png)
![Attribut-Mapper](doc/images/hardcoded-attr-2.png)

## Einschränkungen

Das Plugin kann nicht verwendet werden, wenn die Authentifizierung mittels eines HTTP-Redirects durchgeführt werden soll. Die Art der Implementierung in Keycloak sorgt aktuell dafür, dass für HTTP-Redirects im Extension-Knonten nur die Informationen zur Signatur stehen, wenn der gesamte Inhalt des SAML-Requests signiert werden soll.

Das Plugin bricht deswegen die Authentifizierung grundsätzlich mit einer Fehlermeldung ab, wenn der Authentifizierungs-Request per HTTP-Redirect durchgeführt werden soll.

## Mögliche Erweiterungen

Die Schnittstellen-Spezifikation von BundID sieht neben den Angaben zu den Nutzerdaten-Typen auch Angaben zu den erlaubten Authentifizierungs-Methoden im Extension-Knoten des Request-XMLs vor. Sollte eine Implementierung für das Hinzufügen solcher Angaben benötigt werden, so müsste diese ebenso in einem _SamlAuthenticationPreProcessor_ vorgenommen werden. (Es bietet sich an, den _BundIDAuthenticationPreProcessor_ entsprechend zu erweitern.)
