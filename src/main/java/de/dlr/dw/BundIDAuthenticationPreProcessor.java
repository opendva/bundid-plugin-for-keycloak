/*
 * Copyright 2024 German Aerospace Center (DLR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dlr.dw;

import java.io.StringWriter;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.jboss.logging.Logger;
import org.keycloak.Config.Scope;
import org.keycloak.broker.provider.HardcodedAttributeMapper;
import org.keycloak.broker.saml.mappers.UserAttributeMapper;
import org.keycloak.dom.saml.v2.protocol.AuthnRequestType;
import org.keycloak.dom.saml.v2.protocol.ExtensionsType;
import org.keycloak.models.IdentityProviderMapperModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.KeycloakSessionFactory;
import org.keycloak.models.RealmModel;
import org.keycloak.protocol.saml.preprocessor.SamlAuthenticationPreprocessor;
import org.keycloak.saml.processing.api.saml.v2.request.SAML2Request;
import org.keycloak.sessions.AuthenticationSessionModel;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Extends SAML authentication request with BundID specific attributes.
 * 
 * @author Tim.Fritzsche@dlr.de
 */
public class BundIDAuthenticationPreProcessor implements SamlAuthenticationPreprocessor {

    private static final Logger log = Logger.getLogger(BundIDAuthenticationPreProcessor.class);

    private static final String NAMESPACE_URI_AKDB = "https://www.akdb.de/request/2018/09";
    private static final String NAMESPACE_URI_CLASSIC_UI = "https://www.akdb.de/request/2018/09/classic-ui/v1";

    @Override
    public void close() {
    }

    @Override
    public SamlAuthenticationPreprocessor create(KeycloakSession session) {
        return null;
    }

    @Override
    public void init(Scope config) {
    }

    @Override
    public void postInit(KeycloakSessionFactory factory) {
    }

    @Override
    public String getId() {
        return "bundid";
    }

    @Override
    public AuthnRequestType beforeSendingLoginRequest(AuthnRequestType authnRequest, AuthenticationSessionModel clientSession) {
        RealmModel realm = clientSession.getRealm();
        String alias = realm.getAttribute(BundIDIdentityProvider.CURRENT_ALIAS);
        if (alias != null) {
            try {
                /*
                 * @formatter:off
                 * BundID specific extension of authnRequest. The requested attributes which shall be delivered by the response
                 * have to be specified in the extensions node of the request. Moreover, some display information for the user
                 * are required.
                 * 
                 * Structure:
                 *  <[ns]:Extensions>
                 *      <akdb:AuthenticationRequest xmlns:akdb="https://www.akdb.de/request/2018/09" Version="2">
                 *          <akdb:RequestedAttributes>
                 *              <akdb:RequestedAttribute Name="[urn of first attribute]" />
                 *              <akdb:RequestedAttribute Name="[urn of second attribute]" />
                 *              ...
                 *          </akdb:RequestedAttributes>
                 *          <akdb:DisplayInformation>
                 *              <classic-ui:Version xmlns:classic-ui="https://www.akdb.de/request/2018/09/classic-ui/v1">
                 *                  <classic-ui:OrganizationDisplayName>
                 *                      <![CDATA[Name of organization or useful subject-specific information (max. 50 characters)]]>
                 *                  </classic-ui:OrganizationDisplayName>
                 *                  <classic-ui:OnlineServiceId>
                 *                      <![CDATA[BMI-ID]]>
                 *                  </classic-ui:OnlineServiceId>
                 *              </classic-ui:Version>
                 *          </akdb:DisplayInformation>
                 *      </akdb:AuthenticationRequest>
                 *  </[ns]:Extensions>
                 *  @formatter:on
                 */

                Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument();
                Element authReqEl = doc.createElementNS(NAMESPACE_URI_AKDB, "akdb:AuthenticationRequest");
                authReqEl.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:akdb", NAMESPACE_URI_AKDB);
                authReqEl.setAttributeNS("", "Version", "2");

                Element reqAttsEl = doc.createElementNS(NAMESPACE_URI_AKDB, "akdb:RequestedAttributes");
                authReqEl.appendChild(reqAttsEl);

                realm.getIdentityProviderMappersByAliasStream(alias).filter(m -> UserAttributeMapper.PROVIDER_ID.equals(m.getIdentityProviderMapper())).forEach(m -> {
                    Element reqAttEl = doc.createElementNS(NAMESPACE_URI_AKDB, "akdb:RequestedAttribute");
                    reqAttEl.setAttributeNS("", "Name", m.getConfig().get(UserAttributeMapper.ATTRIBUTE_NAME));
                    reqAttsEl.appendChild(reqAttEl);
                });

                createDisplayInfoElement(doc, realm.getIdentityProviderMappersByAliasStream(alias)).ifPresent(authReqEl::appendChild);

                ExtensionsType extensions = authnRequest.getExtensions();
                if (extensions == null)
                    authnRequest.setExtensions(extensions = new ExtensionsType());

                extensions.addExtension(authReqEl);

                if (log.isDebugEnabled()) {
                    try {
                        // This logging is thought for the check of the extension part. It is not the logging of the final request XML!
                        StringWriter sw = new StringWriter();
                        Transformer transformer = TransformerFactory.newInstance().newTransformer();
                        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
                        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
                        transformer.transform(new DOMSource(SAML2Request.convert(authnRequest)), new StreamResult(sw));
                        log.debug(sw);
                    } catch (Exception e) {
                        log.debug("Logging of request extensions failed", e);
                    }
                }
            } catch (Exception e) {
                throw e instanceof RuntimeException ex ? ex : new RuntimeException(e);
            }
        }

        return authnRequest;
    }

    private Optional<Element> createDisplayInfoElement(Document doc, Stream<IdentityProviderMapperModel> mappers) {
        Element dispInfEl = null;

        Map<String, String> dispInfoAtts = mappers.filter(m -> HardcodedAttributeMapper.PROVIDER_ID.equals(m.getIdentityProviderMapper()))
                .collect(Collectors.toMap(m -> m.getConfig().get(HardcodedAttributeMapper.ATTRIBUTE), m -> m.getConfig().get(HardcodedAttributeMapper.ATTRIBUTE_VALUE)));
        String displayName = dispInfoAtts.get("displayName");
        String bmiId = dispInfoAtts.get("bmiId");

        if ((displayName != null && !displayName.isBlank()) || (bmiId != null && !bmiId.isBlank())) {
            dispInfEl = doc.createElementNS(NAMESPACE_URI_AKDB, "akdb:DisplayInformation");
            Element uiVersionEl = doc.createElementNS(NAMESPACE_URI_CLASSIC_UI, "classic-ui:Version");
            uiVersionEl.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns:classic-ui", NAMESPACE_URI_CLASSIC_UI);
            dispInfEl.appendChild(uiVersionEl);

            if (displayName != null && !displayName.isBlank()) {
                Element orgNameEl = doc.createElementNS(NAMESPACE_URI_CLASSIC_UI, "classic-ui:OrganizationDisplayName");
                orgNameEl.appendChild(doc.createCDATASection(displayName));
                uiVersionEl.appendChild(orgNameEl);
            }

            if (bmiId != null && !bmiId.isBlank()) {
                Element serviceIdEl = doc.createElementNS(NAMESPACE_URI_CLASSIC_UI, "classic-ui:OnlineServiceId");
                serviceIdEl.appendChild(doc.createCDATASection(bmiId));
                uiVersionEl.appendChild(serviceIdEl);
            }
        }

        return Optional.ofNullable(dispInfEl);
    }
}
