/*
 * Copyright 2024 German Aerospace Center (DLR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dlr.dw;

import org.jboss.logging.Logger;
import org.keycloak.broker.provider.AuthenticationRequest;
import org.keycloak.broker.saml.SAMLIdentityProvider;
import org.keycloak.broker.saml.SAMLIdentityProviderConfig;
import org.keycloak.models.KeycloakSession;
import org.keycloak.models.RealmModel;
import org.keycloak.models.utils.ModelToRepresentation;
import org.keycloak.saml.validators.DestinationValidator;

import jakarta.ws.rs.core.Response;

/**
 * @author Tim.Fritzsche@dlr.de
 */
public class BundIDIdentityProvider extends SAMLIdentityProvider {

    private static final Logger log = Logger.getLogger(BundIDIdentityProvider.class);

    static final String CURRENT_ALIAS = "currentAlias";
    static {
        ModelToRepresentation.REALM_EXCLUDED_ATTRIBUTES.add(CURRENT_ALIAS);
    }

    public BundIDIdentityProvider(KeycloakSession session, SAMLIdentityProviderConfig config, DestinationValidator destinationValidator) {
        super(session, config, destinationValidator);
    }

    @Override
    public Response performLogin(AuthenticationRequest request) {
        // The AuthnRequestType will be extended in the BundIDAuthenticationPreProcessor. The problem is that the standard code
        // ignores the extensions of the AuthnRequestType in REDIRECT binding case when the whole SAML protocol message shall be
        // signed (see super#performLogin and the handling of extensions in SAML2AuthnRequestBuilder#createAuthnRequest).
        // Currently (Keycloak version 24.0.0), there is no other way than to abandon the authentication.
        if (!getConfig().isPostBindingAuthnRequest()) {
            log.error("BundID authentication requires special request extensions which cannot be done for an HTTP-Redirect request."
                    + " Please use this plugin only for HTTP-Post request authentication");
            throw new UnsupportedOperationException("HTTP-Redirect ist not supported for BundID authentication.");
        }

        // There is no way to get the alias in the BundIDAuthenticationPreProcessor over standard ways. However, it is needed there.
        // That is why the alias is set temporarily on the realm.
        RealmModel realm = request.getRealm();
        realm.setAttribute(CURRENT_ALIAS, getConfig().getAlias());

        try {
            return super.performLogin(request);
        } finally {
            realm.removeAttribute(CURRENT_ALIAS);
        }
    }
}
