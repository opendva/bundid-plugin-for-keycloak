/*
 * Copyright 2024 German Aerospace Center (DLR)
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package de.dlr.dw;

import org.keycloak.Config;
import org.keycloak.broker.saml.SAMLIdentityProviderConfig;
import org.keycloak.broker.saml.SAMLIdentityProviderFactory;
import org.keycloak.models.IdentityProviderModel;
import org.keycloak.models.KeycloakSession;
import org.keycloak.saml.validators.DestinationValidator;

public class BundIDIdentityProviderFactory extends SAMLIdentityProviderFactory {

    // This field is needed because the analog field in the SAMLIdentityProviderFactory is private (Keycloak version 24.0.0).
    // Delete this line in case the modifier of destinationValidator in SAMLIdentityProviderFactory is changed to protected.
    private DestinationValidator destinationValidator;

    @Override
    public String getName() {
        return "BundID";
    }

    @Override
    public BundIDIdentityProvider create(KeycloakSession session, IdentityProviderModel model) {
        return new BundIDIdentityProvider(session, new SAMLIdentityProviderConfig(model), destinationValidator);
    }

    @Override
    public void init(Config.Scope config) {
        super.init(config);

        // Delete this line when there is no destinationValidator field declared in this class any more.
        this.destinationValidator = DestinationValidator.forProtocolMap(config.getArray("knownProtocols"));
    }
}
